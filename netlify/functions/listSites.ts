import axios from 'axios';
import { uniqBy } from 'lodash';
import { Handler } from '@netlify/functions';

const handler: Handler = async (event, context) => {
  const config = {
    method: 'get',
    url: `https://api.netlify.com/api/v1/sites/${process.env.NETLIFY_SITE_ID}/deploys`,
    headers: {
      Authorization: `Bearer ${process.env.NETLIFY_TOKEN}`,
    },
  };

  return axios(config)
    .then((res) => {
      const data = uniqBy(
        res.data.filter(({ context }) => context !== 'production'),
        ({ branch }) => branch,
      );
      return {
        statusCode: 200,
        body: JSON.stringify({ data }),
      };
    })
    .catch((error) => {
      return {
        statusCode: 200,
        body: JSON.stringify({
          message: error?.message,
          data: [],
        }),
      };
    });
};

export { handler };
